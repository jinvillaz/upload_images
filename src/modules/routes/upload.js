import fs from 'fs';
import path from 'path';
import log4js from 'log4js';
import { Resize } from './resize';
import { config } from '../../config';
const sizes = [30, 256, 500];
const logger = log4js.getLogger('App');

/**
 * Module for register api.
 */
class ApiImages {

  static async uploadImage(req, res, next) {
    try {   
      const name = req.body.name;
      const imgdata = req.body.image;
      const base64Data = imgdata.split(';base64,').pop();
      const imagePath = path.resolve(path.join(config.PATH_IMAGES, 'images'));
      const filePath = path.join(imagePath, `${name}.png`);
      if (fs.existsSync(filePath)) {
        const error = new Error(`The image with name '${name}' already exist.`);
        error.status = 400;
        return next(error);
      }
      fs.writeFileSync(filePath, base64Data, { encoding: 'base64' });
      const imagesResized = await Promise.all(sizes.map(async (size) => {
        const resizer = new Resize(imagePath);        
        return await resizer.save(filePath, name, size);
      }));
      return res.status(200).json({ images: imagesResized });
    } catch (e) {
      next(e);
    }
  }
}

export { ApiImages };
