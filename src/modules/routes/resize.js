import sharp from 'sharp';
import path from 'path';

export class Resize {
  constructor(folder) {
    this.folder = folder;
  }

  async save(buffer, name, size) {
    const filename = `${name}-${size}.png`;
    const filepath = this.filepath(filename);

    await sharp(buffer)
      .resize(size, size, {
        fit: sharp.fit.inside,
        withoutEnlargement: true
      })
      .toFile(filepath);
    
    return filename;
  }

  filepath(filename) {
    return path.resolve(`${this.folder}/${filename}`);
  }
}
