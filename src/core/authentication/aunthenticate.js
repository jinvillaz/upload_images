import jwt from 'jsonwebtoken';
import { config } from '../../config';

const signToken = (user) => {
  return jwt.sign({
    iss: 'black',
    sub: user.id,
    iat: new Date().getTime(),
    exp: new Date().setDate(new Date().getDate() + 1)
  }, config.JWT_STRATEGY_SECRET);
};

export class Authenticate {

  static async googleOAuth(req, res, next) {// eslint-disable-line no-unused-vars
    const token = signToken(req.user);
    res.status(200).json({ token });
  }

  static async getUser(req, res, next) {// eslint-disable-line no-unused-vars
    res.json({ user: req.user });
  }
}
