import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const userSchema = new Schema({
  method: {
    type: String,
    enum: ['google', 'facebook'],
    required: true
  },
  name: String,  
  data: {
    id: {
      type: String
    },
    email: {
      type: String,
      lowercase: true
    },
    picture: {
      type: String
    }
  }
});

const User = mongoose.model('user', userSchema);
export { User };
