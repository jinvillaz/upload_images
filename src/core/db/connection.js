import mongoose from 'mongoose';
import log4js from 'log4js';
import { config } from '../../config';

mongoose.Promise = global.Promise;
const logger = log4js.getLogger('DbConnection');

/**
 * Module for manage the connection with mongodb.
 */
export class DbConnection {

  constructor() {
    this.urlConnection = config.MONGODB_URL;
    //this.connection = mongoose.connection;
  }

  /**
   * Connects with mongodb.
   * @returns {Object} mongoose object.
   */
  async connect() {
    let res;
    try {
      res = await mongoose.connect(this.urlConnection, { useNewUrlParser: true });
    } catch (error) {
      logger.error('Failed database connection.');
      return error;
    }
    logger.info('Successful database connection.');
    return res;
  }

  /**
   * Disconnects from mongodb.
   * @returns {Boolean} disconnection status.
   */
  async disconnect() {
    try {
      await mongoose.disconnect();
    } catch (error) {
      logger.error('Failed database disconnection.');
      return error;
    }
    logger.info('Successful database disconnection.');
    return true;
  }
}
