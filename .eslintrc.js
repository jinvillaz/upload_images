module.exports = {
  'root': true,
  "plugins": [
    'node',
    'jest',
    'prettier'
  ],
  'env': {
    'node': true,
    'jest': true,
    'es6': true
  },
  'parserOptions': {
    'ecmaVersion': 2018,
    'sourceType': 'module'
  },
  'extends': [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings'
  ],
  'parser': 'babel-eslint',
  'rules': {
    'max-params': 0,
    'no-console': [
      2,
      {
        'allow': [
          'info',
          'error'
        ]
      }
    ],
    'max-len': [
      2,
      120,
      2,
      {
        'ignoreComments': true,
        'ignoreUrls': true
      }
    ],
    'semi': [
      'error',
      'always',
      {
        'omitLastInOneLineBlock': true
      }
    ],
    'space-before-function-paren': [
      2,
      {
        'anonymous': 'never',
        'named': 'never'
      }
    ],
    'indent': [
      'error',
      2,
      {
        'MemberExpression': 'off'
      }
    ],
    'newline-per-chained-call': [
      'error',
      {
        'ignoreChainWithDepth': 2
      }
    ],
    'valid-jsdoc': [
      'error',
      {
        'requireReturn': false
      }
    ],
    'arrow-parens': [
      'error',
      'always'
    ],
    'prefer-arrow-callback': 'error',
    'object-curly-spacing': [
      'error',
      'always'
    ],
    'linebreak-style': 0,
    'no-new': 0,
    'radix': 0,
    'max-depth': [
      'error',
      5
    ],
    'no-useless-escape': 0
  }
};
