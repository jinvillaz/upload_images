# Authenthication with google + vuejs app
app in node js
express + vuejs
google authentication
upload files

## Installation

### Global Dev dependencies

```
nodejs 10.16.0
npm install -g yarn
npm install copyfiles -g
npm install rimraf -g
npm install @vue/cli -g
```

### install dependecies
yarn

## Before to start
Before to start you need to create .env file and define  
the next values, where PORT it's optional.

```
PORT=4000  
JWT_STRATEGY_SECRET=mysecret  
OUTH_GOOGLE_CLIENT_ID=mygoogleclient  
OUTH_GOOGLE_CLIENT_SECRET=mygoogleclientsecret
``` 

or you can edit src/config.json and define  
the same values.


## Yarn tasks

  - **yarn build** Generates a build production dist.
  - **yarn dev** Starts the application and watches the files.
  - **yarn start** Generates a build production dist and run the server.
  - **yarn test** Executes all the available test cases.
  - **yarn eslint** Starts the linting utilty eslint.

## Contributors
Jhonatan Villanueva <jinvillaz@gmail.com>

## Repository

https://gitlab.com/jinvillaz/upload_images.git
